# Prerequisites
+ Install pyDelta via pip (**use development version like 0.4dev59**) or https://pydelta.readthedocs.io/en/latest/installation.html
+ argparse is needed for option parsing

# Usage
 `python3 clean input.smt -o output.smt` or `python3 clean input.smt` to write to std  
Run `python3 clean -h` for additional usage tips
</br>
</br>
</br>
</br>
Script has been tested with Python 3.10.0b1
