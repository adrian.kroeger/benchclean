from pydelta.parser import parse_smtlib

def _variable_generator(pref):
	i = 0
	closed = False
	while not closed:
		try:
			yield pref+str(i)
			i += 1
		except GeneratorExit:
			closed = True
			

str_vars  = _variable_generator('str')
int_vars  = _variable_generator('int')
bool_vars = _variable_generator('bool')

def _woorpje_variable_generator():
	i = ord('A')
	closed = False
	while not closed:
		try:
			yield chr(i)
			i += 1
		except GeneratorExit:
			closed = True
			
			
woorpje_vars = _woorpje_variable_generator()
