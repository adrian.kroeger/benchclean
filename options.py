from argparse import ArgumentParser, SUPPRESS

parser = ArgumentParser()
parser.add_argument('file', help='word equation file', metavar='inputfile')
parser.add_argument('-v', '--version', action='version', version=' BenchClean 0.1', help=SUPPRESS)
parser.add_argument('-w', '--woorpje', action='store_true', help='support for woorpje file format')
parser.add_argument('-o', metavar='outfile')

# transform options
tf_group = parser.add_argument_group('transform options')
tf_group.add_argument('-t', '--transforms', nargs='+', default=[], 
help='choose from following transformations: all, high_order, sep_eq, join_cat, rename_vars, none')
tf_group.add_argument('--keep-model', action='store_true', help='update model to only include original variables after transformations are applied')
