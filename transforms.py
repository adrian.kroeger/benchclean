from utils import str_vars, int_vars, bool_vars


def contains(a, b):
	a_ = next(str_vars)
	b_ = next(str_vars)
	c_ = next(bool_vars)
	x1 = next(str_vars)
	x2 = next(str_vars)
	
	return c_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {x2} () String)\n'
			f'(declare-fun {c_} () Bool)'),(
			f'(and (= {a_} {a})\n'
			f'(= {b_} {b})\n'
			f'(= {c_} (= {a_} (str.++ {x1} {b_} {x2}))))')

		   
def prefixof(a, b):
	a_ = next(str_vars)
	b_ = next(str_vars)
	c_ = next(bool_vars)
	x1 = next(str_vars)
	
	return c_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {c_} () Bool)'),(
			f'(and (= {a_} {a})\n'
			f'(= {b_} {b})\n'
			f'(= {b_} (str.++ {a_} {x1})))')
		   

def suffixof(a, b):
	a_ = next(str_vars)
	b_ = next(str_vars)
	c_ = next(bool_vars)
	x1 = next(str_vars)
	
	return c_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {c_} () Bool)'),(
			f'(and (= {a_} {a})\n'
			f'(= {b_} {b})\n'
			f'(= {b_} (str.++ {x1} {a_})))')
		   
		   
def at(a, i):
	a_ = next(str_vars)
	b_ = next(str_vars)
	i_ = next(int_vars)
	x1 = next(str_vars)
	x2 = next(str_vars)
	
	return b_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {i_} () Int)\n'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {x2} () String)\n'),(
			f'(and (= {a_} {a})\n'
			f'(= {i_} {i})\n'
			f'(ite (and (<= 0 {i_}) (< {i_} (str.len {a_})))\n'
			f'(and \n'
			f'(= {a_} (str.++ {x1} {b_} {x2})) \n'
			f'(= (str.len {x1}) {i_})\n'
			f'(= (str.len {b_}) 1)\n'
			f'(= (str.len {x2}) (- (str.len {a_}) {i_} 1))\n'
			f')\n'
			f'(= {b_} "")\n'
			f'))\n')
			
			
def substr(a, i, n):
	a_ = next(str_vars)
	b_ = next(str_vars)
	i_ = next(int_vars)
	x1 = next(str_vars)
	x2 = next(str_vars)
	
	return b_, (f'(define-fun max ((a Int) (b Int)) Int (ite (< a b) b a))\n'
			f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {i_} () Int)'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {x2} () String)\n'),(
			f'(and (= {a_} {a})\n'
			f'(= {i_} {i})\n'
			f'(ite (and (<= 0 {i_}) (< {i_} (str.len {a_})) (> {n} 0))\n'
			f'(and\n'
			f'(= {a_} (str.++ {x1} {b_} {x2}))\n'
			f'(= (str.len {x1}) {i_})\n'
			f'(= (str.len {x2}) (max (- (str.len {a_}) (+ {i_} {n})) 0))\n'
			f')\n'
			f'(= {b_} "")\n'
			f'))\n')
	
	
def indexof(a, b, i):
	a_ = next(str_vars)
	b_ = next(str_vars)
	i_ = next(int_vars)
	j_ = next(int_vars)
	x1 = next(str_vars)
	x2 = next(str_vars)
	x3 = next(str_vars)
	x4 = next(str_vars)
	x5 = next(str_vars)
	
	return j_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {i_} () Int)'
			f'(declare-fun {j_} () Int)'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {x2} () String)\n'
			f'(declare-fun {x3} () String)\n'
			f'(declare-fun {x4} () String)\n'
			f'(declare-fun {x5} () String)\n'
			f'(declare-fun {x6} () String)\n'
			f'(declare-fun {x7} () String)\n'
			f'(declare-fun {x8} () String)\n'
			f'(declare-fun {x9} () String)\n'
			f'(declare-fun {x10} () String)\n'
			f'(declare-fun {x11} () String)\n'
			f'(declare-fun {x12} () String)\n'),(
			f'(and (= {a_} {a})\n'
			f'(= {b_} {b})\n'
			f'(= {i_} {i})'
			f'(ite (= {a_} (str.++ {x6} {b_} {x7})))\n'
			f'(and\n'
			f'(= {a_} (str.++ {x1} {x2} {x3}))\n'
			f'(= (str.len {x1}) {i_})\n'
			f'(ite (= {x2} (str.++ {x8} {b_} {x9}))\n'
			f'(and\n'
			f'(= {x2} (str.++ {x4} {x5}))\n'
			f'(not (= {x4} (str.++ {x10} {b_} {x11})))\n'
			f'(= {x5} (str.++ {b_} {x12}))\n'
			f'(= {j_} (+ {i_} (str.len {x4})))\n'
			f')\n'
			f'(= {j_} (- 1))\n'
			f')\n'
			f')\n'
			f'(= {j_} (- 1))\n'
			f'))\n')
			

def replace(a, b, c):
	a_ = next(str_vars)
	b_ = next(str_vars)
	c_ = next(str_vars)
	d_ = next(str_vars)
	x1 = next(str_vars)
	x2 = next(str_vars)
	x3 = next(str_vars)
	x4 = next(str_vars)
	
	return d_, (f'(declare-fun {a_} () String)\n'
			f'(declare-fun {b_} () String)\n'
			f'(declare-fun {c_} () String)\n'
			f'(declare-fun {d_} () String)\n'
			f'(declare-fun {x1} () String)\n'
			f'(declare-fun {x2} () String)\n'
			f'(declare-fun {x3} () String)\n'
			f'(declare-fun {x4} () String)\n'),(
			f'(and (= {a_} {a})\n'
			f'(= {b_} {b})\n'
			f'(= {c_} {c})\n'
			f'(ite (= {b_} "")\n'
			f'(= {d_} (str.++ {a_} {c_}))\n'
			f'(ite (= {a_} (str.++ {x1} {b_} {x2}))\n'
			f'(and\n'
			f'(= {a_} (str.++ {x1} {b_} {x2}))\n'
			f'(not (= {x1} (str.++ {x3} {b_} {x4})))\n'
			f'(= {d_} (str.++ {x1} {c_} {x2}))\n'
			f')\n'
			f'(= {d_} {a_})\n'
			f')\n'
			f'))\n')
			
