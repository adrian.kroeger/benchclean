'''
Class definitions differ from pyDelta standard!
Return value is interpreted as return (subst, funs, eq):
subst -> what to substitute for current node
funs  -> additional variable definitions
eq    -> additional equations
'''
from pydelta.semantics import *
from pydelta import parser
import transforms
import utils


class EqSeparator:

	def filter(self, node):
		return has_name(node) and get_name(node) == '=' and len(node) > 3
	
	
	def mutations(self, node):
		# split n-term equations into n-1 2-term equations
		return ['and']+[['=', node[i], node[i+1]] for i in range(1, len(node)-1)], None, None
			
	
	
	def __str__(self):
		return 'Turns an equation with more than two terms into separate ones'
		
		
class JoinConcats:
	
	def filter(self, node):
		return has_name(node) and get_name(node) == 'str.++' and not all([is_leaf(arg) for arg in node[1:]])
	
	
	def mutations(self, node):
		# recursively join inner concats
		ret = ['str.++']
		for sub in node[1:]:
			if has_name(sub) and get_name(sub) == 'str.++':
				self.mutations(sub)
				ret.extend(sub[1:])
			else:
				ret.append(sub)
		return ret, None, None
		
	
	def __str__(self):
		return 'Flattens compositions of str.++\'s'
		

	
class TransformHighOrder:

	_OPERATIONS = {'str.contains': transforms.contains, 'str.prefixof': transforms.prefixof, 
	'str.suffixof': transforms.suffixof, 'str.at': transforms.at, 'str.substr': transforms.substr, 
	'str.indexof': transforms.indexof, 'str.replace': transforms.replace}
	
	def filter(self, node):
		# confirm that node is higher order operation
		return has_name(node) and get_name(node) in self._OPERATIONS
		
	
	def mutations(self, node):
		op = node[0]
		args = node[1:]
		
		return self._OPERATIONS[op](*args)
		
